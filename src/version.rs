use reqwest::{Url, UrlError};
use semver::Version;

use std::borrow::Borrow;

#[cfg(target_os = "linux")]
const EXT: &'static str = "xz";
#[cfg(target_os = "linux")]
const OS: &'static str = "linux";

#[cfg(target_os = "macos")]
const EXT: &'static str = "gz";
#[cfg(target_os = "macos")]
const OS: &'static str = "darwin";

pub fn into_url<V:Borrow<Version>>(version: V) -> Result<Url, UrlError> {
    let triple = format!("v{}", version.borrow());
    let arch = "x64";
    Url::parse(&format!("https://nodejs.org/dist/{}/node-{}-{}-{}.tar.{}",
           triple,
           triple,
           OS,
           arch,
           EXT))
}

#[cfg(test)]
mod tests {
    use super::*;
    use spectral::prelude::*;

    #[test]
    fn owned_into_url() {
        let url = format!("https://nodejs.org/dist/v4.3.0/node-v4.3.0-{}-x64.tar.{}", OS, EXT);
        assert_that(&Url::parse(&url))
            .is_equal_to(into_url(&Version {
                               major: 4,
                               minor: 3,
                               patch: 0,
                               pre: vec![],
                               build: vec![]
        }));
    }

    #[test]
    fn borrowed_into_url() {
       let url = format!("https://nodejs.org/dist/v5.7.9/node-v5.7.9-{}-x64.tar.{}", OS, EXT);
       let  borrowed_version = &Version {
           major: 5,
           minor: 7,
           patch: 9,
           pre: vec![],
           build: vec![]
        };
        assert_that(&into_url(borrowed_version))
            .is_equal_to(&Url::parse(&url));
    }

    #[test]
    fn display_semver_triple() {
        assert_that(&format!("{}", Version {major:0, minor:10, patch: 39, pre: vec![], build: vec![]}))
            .is_equal_to("0.10.39".to_string());
    }
}
