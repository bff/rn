#[cfg(target_os = "macos")] extern crate flate2;
#[cfg(target_os = "linux")] extern crate lzma;
extern crate reqwest;
extern crate semver;
extern crate serde;
#[macro_use] extern crate serde_derive;
#[cfg(test)] extern crate spectral;
extern crate tar;

use std::error::Error;
use std::fs::File;
use std::io::Write;
use std::path::Path;

use self::cache::Cache;
use self::client::ReqwestClient;
use self::decompressor::into_archive;
use self::default::set_default;
use self::download::download;
use self::input::Input;
use self::package_json::{parse_versions, Engines};
use self::release::{get_releases_file, resolve};
use self::shell::update_path;

mod cache;
mod client;
mod decompressor;
mod default;
mod download;
mod input;
mod package_json;
mod release;
mod shell;
mod version;

fn stderr<T: Into<String>>(msg: T) {
    let msg = msg.into();
    let ref mut to_stderr = &mut ::std::io::stderr();
    writeln!(to_stderr, "{}", &msg).unwrap();
}

fn run() -> Result<String, Box<Error>> {
    let input = Input::parse();
    let cache = Cache::new(&input)?;

    if input.args.len() < 2 {
        return Err(From::from("USAGE: rn --list\nUSAGE: rn <path to package.json>"));
    }
    let ref first_arg = input.args[1];

    // TODO -- reorganize with CLAP
    // COMMAND: set default nodejs binary using symlinks
    if "--default" == first_arg {
        if input.args.len() == 3 {
            set_default(&input.args[2], &cache)?;
            return Ok(format!("set default to {}", input.args[2]));
        } else {
            return Err(From::from("Invalid command -- use rn --default <version>"));
        }
    }

    // COMMAND: list installed versions
    if "--list" == first_arg {
        for version in cache.iter() {
            println!("{}", &version);
        }
        return Ok(String::new());
    }

    // COMMAND: setup node environment for given package.json
    let package_path = Path::new(first_arg);
    if !package_path.exists() || !package_path.is_file() {
        return Err(From::from("no package.json file found at path"));
    }

    let package_json = File::open(package_path)?;
    let Engines {node: node_version_req, ..} = parse_versions(package_json)?;
    // TODO -- implement Into<Path> for arguments to get_releases_file()
    let releases = get_releases_file(Path::new("/tmp/node-releases.tab"))?;
    let node_version = resolve(node_version_req, releases)?;
    stderr(format!("rn is setting up node v{} in this shell", &node_version));


    // FIXME Dry this code out! Dupe in default.rs
    // Download nodejs binaries from interwebs if not already downloaded
    if !cache.contains(&node_version) {
        let response = download(&node_version, &ReqwestClient::new())?;

        // Write it to disk
        let destination = cache.path(&node_version);
        let mut archive = into_archive(response)?;
        for file in archive.entries()? {
            let mut file = file?;
            file.unpack_in(&destination)?;
        }
    }

    // Calculate and output updated PATH
    if let Some(ref path) = input.envvars.get("PATH") {
        if let Some(ref rn_dir) = input.version_directory().to_str() {
            return Ok(update_path(path, rn_dir, &node_version));
        } else {
            return Err(From::from("RN is improperly configured. No directory for storing releases of nodejs."));
        }
    }

    Err(From::from("No PATH envvar passed to rn"))
}

fn main() {
    match run() {
        Ok(output) => println!("{}", output),
        Err(err) => {
            stderr(err.description());
            ::std::process::exit(1);
        }
    };
}
