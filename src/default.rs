use std::error::Error;
use std::fs::remove_file;
use std::os::unix::fs::symlink;
use std::path::Path;

use semver::Version;

use ::cache::Cache;
use ::client::ReqwestClient;
use ::decompressor::into_archive;
use ::download::download;

const DEFAULT_NODEJS_PATH: &str = "/usr/local/bin/node";
const ARCH: &str = "x64";

#[cfg(target_os = "macos")]
const OS: &str = "darwin";
#[cfg(target_os = "linux")]
const OS: &str = "linux";

pub fn set_default(node_version: &str, cache: &Cache) -> Result<(), Box<Error>> {
    let node_version = Version::parse(node_version)?;

    // Download nodejs binaries from interwebs if not already downloaded
    if !cache.contains(&node_version) {
        let response = download(&node_version, &ReqwestClient::new())?;

        // Write it to disk
        let destination = cache.path(&node_version);
        let mut archive = into_archive(response)?;
        for file in archive.entries()? {
            let mut file = file?;
            file.unpack_in(&destination)?;
        }
    }

    let mut src = cache.path(&node_version);
    src.push(format!("node-v{}-{}-{}/bin/node", &node_version, OS, ARCH));
    remove_file(DEFAULT_NODEJS_PATH)?;
    symlink(src, DEFAULT_NODEJS_PATH)?;
    Ok(())
}
