///Input structs represent the entire state of the environment that launched the process.
///The idea is to have more or less "pure" functional components that are decoupled from
///the messy state of the world outside the process. This structure acts as a boundary.

use std::collections::HashMap;
use std::env;
use std::path::PathBuf;

const ENVVAR_VERSION_DIRECTORY: &'static str = "RN_DIR";
const ENVVAR_HOME: &'static str = "HOME";
const DEFAULT_VERSION_DIRECTORY: &'static str = "/.rn/versions";

// TODO: make this struct lazily evaluate envvars / arguments
pub struct Input {
    pub envvars: HashMap<String, String>,
    pub args: Vec<String>
}

impl Input {
    pub fn parse() -> Self {
        Input {
            envvars: env::vars().collect(),
            args: env::args().collect()
        }
    }

    // TODO: use Option methods more effectively
    pub fn version_directory(&self) -> PathBuf {
        if let Some(override_path) = self.envvars.get(ENVVAR_VERSION_DIRECTORY) {
            return PathBuf::from(override_path);
        } else if let Some(home) = self.envvars.get(ENVVAR_HOME) {
            let mut default_path = home.to_owned();
            default_path.push_str(DEFAULT_VERSION_DIRECTORY);
            return PathBuf::from(&default_path);
        }
        return PathBuf::from("/tmp/.rn/versions");
    }

}

#[cfg(test)]
mod test {
    use super::*;
    use std::env;

    // ::parse() --------------------------

    #[test]
    fn parses_envvars() {
        let input = Input::parse();
        assert_eq!(input.envvars.len(), env::vars().count());
    }

    #[test]
    fn parses_args() {
        let input = Input::parse();
        assert_eq!(input.args.len(), env::args().count());
    }

    // ::version_directory() ---------------

    #[test]
    fn version_directory_uses_default_path() {
        let home = ::std::env::var(ENVVAR_HOME).unwrap();
        let mut envvars = HashMap::new();
        envvars.insert("HOME".to_string(), home.clone());
        let input = Input{envvars: envvars, args: vec!()};
        assert_eq!(input.version_directory(), PathBuf::from(home + "/.rn/versions"));
    }

    #[test]
    fn version_directory_uses_path_from_env_var() {
        let mut envvars = HashMap::new();
        envvars.insert("RN_DIR".to_string(), "/tmp/.rn/versions".to_string());
        let input = Input{envvars: envvars, args: vec!()};
        assert_eq!(input.version_directory(), PathBuf::from("/tmp/.rn/versions"));
    }

    #[test]
    fn version_directory_has_fallback_to_tmp() {
        let input = Input{envvars: HashMap::new(), args: vec!()};
        assert_eq!(input.version_directory(), PathBuf::from("/tmp/.rn/versions"));
    }
}
