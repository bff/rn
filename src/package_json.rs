extern crate serde_json;
extern crate semver;

use std::fmt::{Display, Formatter};
use std::io::Read;
use std::error::Error;

use self::semver::VersionReq;

#[derive(Debug, PartialEq)]
pub enum ParseError {
    InvalidJson,
    MissingEngines,
    MissingNode,
    InvalidSemver,
}

impl Error for ParseError {
    fn description(&self) -> &str {
        match *self {
            ParseError::InvalidJson => "Invalid JSON in package.json",
            ParseError::MissingEngines => "no \"engines\" specified in package.json",
            ParseError::MissingNode => "no node version specified in package.json",
            ParseError::InvalidSemver => "the semver expression is unsupported or invalid"
        }
    }
}

impl Display for ParseError {
    fn fmt(&self, f: &mut Formatter) -> ::std::fmt::Result {
        write!(f, "{}", self.description())
    }
}

#[derive(Debug, Deserialize)]
struct PackageJson {
    engines: Option<TmpEngines>
}

#[derive(Debug, Deserialize)]
struct TmpEngines {
    pub node: Option<String>,
}

#[derive(Debug, PartialEq)]
pub struct Engines {
  pub node: VersionReq,
  pub npm: Option<VersionReq>
}

pub fn parse_versions<R: Read>(raw_json: R) -> Result<Engines, ParseError> {
    // TODO -- look at error chain, or otherwise preserve the serde error information
    let package_json: PackageJson = serde_json::from_reader(raw_json)
        .map_err(|_| ParseError::InvalidJson)?;

    let engines = package_json.engines;
    if engines.is_none() {
        return Err(ParseError::MissingEngines);
    }

    let TmpEngines { node: node_option } = engines.unwrap();
    if node_option.is_none() {
      return Err(ParseError::MissingNode);
    }
    let node = VersionReq::parse(&node_option.unwrap())
        .map_err(|_| ParseError::InvalidSemver)?;

    Ok(Engines{node, npm: None})
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn errors_on_invalid_json() {
        assert_eq!(parse_versions("{".as_bytes()), Err(ParseError::InvalidJson));
    }

    #[test]
    fn errors_if_engines_missing() {
        assert_eq!(parse_versions("{}".as_bytes()),
                   Err(ParseError::MissingEngines));
    }

    #[test]
    fn errors_if_node_missing() {
        assert_eq!(parse_versions(r#"{"engines":{"npm":"3.9.7"}}"#.as_bytes()),
                   Err(ParseError::MissingNode));
    }

    #[test]
    fn parses_node_version() {
        let expected_req = VersionReq::parse("6.1.0").unwrap();
        assert_eq!(parse_versions(r#"{"engines":{"node":"6.1.0"}}"#.as_bytes()),
                   Ok(Engines{node: expected_req, npm: None}));
    }
}
