extern crate reqwest;

#[cfg(test)]
use std::cell::{Cell, RefCell};

use std::io::Read;
use reqwest::{Url, Result, Response};

/// A trait so we test download logic without using a real network.
pub trait HttpClient<R: Read> {
    fn get(&self, url: Url) -> Result<R>;
}

/// A (really) thin wrapper around reqwest::get().
pub struct ReqwestClient;

impl ReqwestClient {
    pub fn new() -> Self {
        ReqwestClient
    }
}

impl HttpClient<Response> for ReqwestClient {
    fn get(&self, url: Url) -> Result<Response> {
        let response = reqwest::get(url).unwrap();
        Ok(response)
    }
}

/// A test double for reqwest::get()
#[cfg(test)]
pub struct FakeHttpClient {
    reply: &'static [u8],
    pub request_url: RefCell<Option<String>>,
    pub call_count: Cell<i8>,
}

#[cfg(test)]
impl FakeHttpClient {
    pub fn replies(body: &'static str) -> Self {
        FakeHttpClient {
            reply: body.as_bytes(),
            request_url: RefCell::new(None),
            call_count: Cell::new(0),
        }
    }
}

#[cfg(test)]
impl HttpClient<&'static [u8]> for FakeHttpClient {
    fn get(&self, url: Url) -> Result<&'static [u8]> {
        *self.request_url.borrow_mut() = Some(url.into_string());
        let last_count = self.call_count.get();
        self.call_count.set(last_count + 1);
        Ok(self.reply)
    }
}
