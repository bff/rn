#[cfg(target_os = "linux")]
extern crate lzma;
#[cfg(target_os = "macos")]
extern crate flate2;

use std::error::Error;
use std::io::Read;

use tar::Archive;

#[cfg(target_os = "linux")]
use lzma::LzmaReader;
#[cfg(target_os = "macos")]
use flate2::FlateReadExt;
#[cfg(target_os = "macos")]
use flate2::read::GzDecoder;

#[cfg(target_os = "linux")]
pub fn into_archive<R: Read>(compressed: R) -> Result<Archive<LzmaReader<R>>, Box<Error>> {
    let decompressor = LzmaReader::new_decompressor(compressed)?;
    Ok(Archive::new(decompressor))
}

#[cfg(target_os = "macos")]
pub fn into_archive<R: Read>(compressed: R) -> Result<Archive<GzDecoder<R>>, Box<Error>> {
    let decoder = compressed.gz_decode()?;
    Ok(Archive::new(decoder))
}
