use std::{fs,io};
use std::path::PathBuf;
use std::collections::HashSet;
use std::collections::hash_set::Iter;

use semver::Version;
use ::input::Input;

pub struct Cache {
    versions: HashSet<Version>,
    path: PathBuf
}

impl Cache {
    // TODO: consider making this struct lazily fetch state from environment
    pub fn new(input: &Input) -> io::Result<Self> {
        let version_directory = input.version_directory();
        if !version_directory.exists() {
            fs::create_dir_all(&version_directory)?;
        }

        // Try to parse each subdirectory name as a node version
        let mut versions = HashSet::new();
        for entry in try!(fs::read_dir(&version_directory)) {
            if let Ok(dir) = entry {
                if let Some(dir_str) = dir.file_name().to_str() {
                    if let Ok(version) = Version::parse(dir_str) {
                        versions.insert(version);
                    }
                }
            }
        }

        // Cache the version directory for later
        let path = PathBuf::from(&version_directory);
        Ok(Cache{versions, path})
    }

    pub fn contains(&self, version: &Version) -> bool {
        self.versions.contains(version)
    }

    pub fn iter(&self) -> Iter<Version> {
        self.versions.iter()
    }

    pub fn path(&self, version: &Version) -> PathBuf {
        let mut version_path = self.path.clone();
        version_path.push(format!("{}", version));
        version_path
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashMap;

    #[test]
    fn new_parses_node_versions_from_path() {
        let mut envvars = HashMap::new();
        envvars.insert("RN_DIR".to_string(), "/tmp/.rn/versions".to_string());
        let input = Input{envvars: envvars, args: vec!()};
        assert!(fs::create_dir_all("/tmp/.rn/versions/4.3.0").is_ok());
        assert!(fs::create_dir_all("/tmp/.rn/versions/6.1.0").is_ok());

        let Cache{versions, ..} = Cache::new(&input).unwrap();

        assert!(versions.contains(&Version{major:4, minor:3, patch:0, pre:vec![], build:vec![]}));
        assert!(versions.contains(&Version{major:6, minor:1, patch:0, pre:vec![], build:vec![]}));
    }

    #[test]
    fn path_combines_version_directory_and_node_version() {
        let cache = Cache {versions: HashSet::new(), path: PathBuf::from("/home/bryce/.rn/versions")};
        let version = Version::parse("6.2.1").unwrap();
        assert_eq!(PathBuf::from("/home/bryce/.rn/versions/6.2.1"), cache.path(&version));
    }
}
