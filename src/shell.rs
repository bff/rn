use semver::Version;

#[cfg(target_os = "linux")]
const OS: &'static str = "linux";
#[cfg(target_os = "macos")]
const OS: &'static str = "darwin";

pub fn update_path<'a>(path: &'a str, rn_dir: &'a str, version: &'a Version) -> String {
    let version_dir = format!("|||{}/{}.{}.{}/node-v{}.{}.{}-{}-x64/bin",
            rn_dir,
            version.major,
            version.minor,
            version.patch,
            version.major,
            version.minor,
            version.patch,
            OS);
    path.split(':')
        .filter(|segment| !segment.contains(rn_dir))
        .fold(version_dir, |path, segment| path + ":" + segment)
}

#[cfg(test)]
mod tests {
    use super::*;
    use spectral::prelude::*;

    #[test]
    fn adds_rn_node_to_envvar_path() {
        let path = "/usr/bin:/usr/local/bin:/bin";
        let rn_dir = "/home/bff/.rn/versions";
        let version = Version {major:7, minor:3, patch:1, pre: vec![], build: vec![]};

        let actual = &update_path(path, rn_dir, &version);
        let expected = &format!("|||/home/bff/.rn/versions/7.3.1/node-v7.3.1-{}-x64/bin:/usr/bin:/usr/local/bin:/bin", OS);
        assert_that(&actual).is_equal_to(&expected);
    }

    #[test]
    fn replaces_rn_dir_in_envvar_path() {
        let path = "/home/bff/.rn/versions/7.3.1:/usr/bin:/usr/local/bin:/bin";
        let rn_dir = "/home/bff/.rn/versions";
        let version = Version {major:4, minor:5, patch:0, pre: vec![], build: vec![]};

        let actual = &update_path(path, rn_dir, &version);
        let expected = &format!("|||/home/bff/.rn/versions/4.5.0/node-v4.5.0-{}-x64/bin:/usr/bin:/usr/local/bin:/bin", OS);
        assert_that(&actual).is_equal_to(&expected);
    }
}
