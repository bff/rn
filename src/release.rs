extern crate csv;

use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::io;
use std::path::Path;

use reqwest;
use semver::{Version, VersionReq};

pub const RELEASES_URL: &str = "https://nodejs.org/dist/index.tab";
const ONE_DAY: u64 = 24 * 60 * 60;

#[derive(Deserialize, Debug, PartialEq)]
struct Release {
    version: String,
}

fn should_download(path: &Path) -> bool {
    if !path.exists() {
        return true;
    }

    // TODO -- make a macro for this pattern. So much boilerplate....
    let file = File::open(path);
    if file.is_err() {
        return true;
    }
    let file = file.unwrap();

    let metadata = file.metadata();
    if metadata.is_err() {
        return true;
    }
    let metadata = metadata.unwrap();

    let modified = metadata.modified();
    if modified.is_err() {
        return true;
    }
    let modified = modified.unwrap();

    let elapsed = modified.elapsed();
    if elapsed.is_err() {
        return true;
    }
    let elapsed = elapsed.unwrap();

    return elapsed.as_secs() > ONE_DAY;
}

pub fn get_releases_file(path: &Path) -> Result<File, Box<Error>> {
    let mut file;

    if should_download(path) {
        let mut response = reqwest::get(RELEASES_URL)?;
        if !response.status().is_success() {
            return Err(From::from(format!("Unexpected response from nodejs.org while downloading releases: {:?}", &response)));
        }
        file = File::create(path)?;
        io::copy(&mut response, &mut file)?;
    } else {
        file = File::open(path)?;
    }

    Ok(file)
}

pub fn resolve<R:Read>(req: VersionReq, releases: R) -> Result<Version,Box<Error>> {
    let mut rdr = csv::ReaderBuilder::new()
        .delimiter(b'\t')
        .from_reader(releases);

    for result in rdr.deserialize() {
        if result.is_ok() {
            let release: Release = result.unwrap();
            if let Ok(version) = Version::parse(&release.version[1..]) {
                if req.matches(&version) {
                    return Ok(version);
                }
            }
        }
    }

    Err(From::from(format!("There are no releases of nodejs that satisfy the given package.json: {}", req)))
}

#[cfg(test)]
mod test {
    use super::*;
    use std::process::Command;
    use spectral::prelude::*;
    const VALID_RELEASES: &str = "version	date	files	npm	v8	uv	zlib	openssl	modules	lts
v8.1.3	2017-06-29	aix-ppc64,headers,linux-arm64,linux-armv6l,linux-armv7l,linux-ppc64le,linux-x64,linux-x86,osx-x64-pkg,osx-x64-tar,src,sunos-x64,sunos-x86,win-x64-exe,win-x64-msi,win-x86-exe,win-x86-msi	5.0.3	5.8.283.41	1.12.0	1.2.11	1.0.2l	57	-";
    const PARTIALLY_INVALID_RELEASES: &str = "version	date	files	npm	v8	uv	zlib	openssl	modules	lts
invalid row
v8.1.3	2017-06-29	aix-ppc64,headers,linux-arm64,linux-armv6l,linux-armv7l,linux-ppc64le,linux-x64,linux-x86,osx-x64-pkg,osx-x64-tar,src,sunos-x64,sunos-x86,win-x64-exe,win-x64-msi,win-x86-exe,win-x86-msi	5.0.3	5.8.283.41	1.12.0	1.2.11	1.0.2l	57	-";

    #[test]
    fn test_recent_release() {
        let mut rdr = csv::ReaderBuilder::new()
            .delimiter(b'\t')
            .from_reader(VALID_RELEASES.as_bytes());
        let record: Release = rdr.deserialize()
            .next()
            .unwrap()
            .unwrap();

        assert_that(&record).is_equal_to(Release {
            version: "v8.1.3".to_string(),
        });
    }

    // TODO -- currently an "integration" test requires internet access. Make this test meaningful offline
    #[test]
    fn run_get_releases() {
        get_releases_file(Path::new("/tmp/rn-release")).unwrap();
    }

    #[test]
    fn should_download_true_when_file_missing() {
        let path_without_file = Path::new("/tmp/does-not-exist");
        if path_without_file.exists() {
          panic!("For test to succeed, file must not exist at {:?}", path_without_file);
        }
        assert!(should_download(path_without_file));
    }

    #[test]
    fn should_download_false_when_new_file_exists() {
        let file_path = Path::new("/tmp/rn-releases-new-file");
        let _ = Command::new("touch")
          .arg("-d")
          .arg("-23 hours")
          .arg(file_path)
          .status()
          .expect("Unable to fudge date for tempfile in test");
        assert!(!should_download(file_path));
    }

    #[test]
    fn should_download_true_when_file_older_than_one_day() {
        let file_path = "/tmp/rn-releases-stale-file";
        let _ = Command::new("touch")
          .arg("-d")
          .arg("-2 days")
          .arg(file_path)
          .status()
          .expect("Unable to fudge date for tempfile in test");
        assert!(should_download(Path::new(file_path)));
    }

    #[test]
    fn resolve_finds_obvious_match() {
        let req = VersionReq::parse("8.x").unwrap();
        let result = resolve(req, VALID_RELEASES.as_bytes());
        assert_that(&result)
            .is_ok_containing(Version::parse("8.1.3").unwrap())
    }

    #[test]
    fn resolve_skips_invalid_rows() {
        let req = VersionReq::parse("8.x").unwrap();
        let result = resolve(req, PARTIALLY_INVALID_RELEASES.as_bytes());
        assert_that(&result)
            .is_ok_containing(Version::parse("8.1.3").unwrap())
    }

    #[test]
    fn resolve_handles_no_match() {
        let req = VersionReq::parse("7.x").unwrap();
        let result = resolve(req, VALID_RELEASES.as_bytes());
        assert_that(&result).is_err();
    }
}
