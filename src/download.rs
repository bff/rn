use std::io::Read;
use std::error::Error;

use semver::Version;

use ::client::HttpClient;
use ::version::into_url;

pub fn download<R: Read, C: HttpClient<R>>(version: &Version, client: &C) -> Result<R, Box<Error>> {
    let url = into_url(version)?;
    let response = client.get(url)?;
    Ok(response)
}

#[cfg(test)]
mod tests {
    use super::*;
    use ::client::FakeHttpClient;
    use spectral::prelude::*;

    #[test]
    fn writes_response_body_to_destination() {
        // Setup
        let client = FakeHttpClient::replies("node is so dynamic");
        let version = Version {
            major: 1,
            minor: 2,
            patch: 3,
            pre: vec![],
            build: vec![]
        };

        // System under test
        let result = download(&version, &client).unwrap();

        // Expectations
        assert_eq!(1, client.call_count.get());
        assert_eq!(b"node is so dynamic", result);
        assert_that(&format!("{}", into_url(version).unwrap()))
            .is_equal_to(&client.request_url.into_inner().unwrap());
    }

}
