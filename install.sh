#!/bin/bash -eu

bin_dir='/usr/local/bin'

## Helper functions

function abort() {
  echo "${1:-'unexpected error'}" 1>&2
  exit 1
}

function require_tool() {
  local tool="${1:-}"
  type "$tool" &>/dev/null  || abort "$tool must be installed to run this script"
}

## Logical Steps

function verify_supported_os() {
  local os="$(uname)"
  if [[ "$os" == "Darwin" ]]; then
    echo "Installing RN for MacOS"
  elif [[ "$os" == "Linux" ]]; then
    echo "Installing RN for Linux"
  else
    abort "rn does not support $os at this time"
  fi
}

function verify_required_tools() {
  require_tool curl
  require_tool unzip
}

function download_release() {
  # Idempotent -- remove pre-existing archives
  rm -rf /tmp/rn.zip
  curl -sSL \
    -o /tmp/rn.zip \
    'https://gitlab.com/bff/rn/builds/artifacts/master/download?job=release_build'
}

function install_release() {
  local shim_dir="$HOME/.config/rn"
  local tmp_dir='/tmp/rn'

  rm -rf "$shim_dir" "$tmp_dir"
  mkdir "$shim_dir" "$tmp_dir"

  unzip -q -d /tmp/rn /tmp/rn.zip
  mv /tmp/rn/bin/rn.sh "$shim_dir/"

  echo 'Replacing the existing rn binary with the newly downloaded version. This requires root privileges.'
  sudo rm -f "$bin_dir/rn"
  case "$(uname)" in
    'Linux')
      sudo mv "$tmp_dir/bin/linux_x64/rn" "$bin_dir/"
      ;;
    'Darwin')
      sudo mv "$tmp_dir/bin/macos_x64/rn" "$bin_dir/"
      ;;
    *)
      abort 'Unsupported OS! Unable to install RN'
      ;;
  esac
}

function setup_instructions() {
  echo
  echo "------------------------------------"
  echo "Success! RN is installed in $bin_dir"
  echo "------------------------------------"

  (echo "$PATH" | grep "$bin_dir" &>/dev/null) || \
    cat <<-EOF
		
		Sadly, it looks like $bin_dir isn't in your PATH, so RN needs a little more setup.
		Add this to your ~/.bashrc (or equivalent file):
		    export PATH="$bin_dir:\$PATH"
EOF

  local init_script='if [[ -r "$HOME/.config/rn/rn.sh" ]]; then source "$HOME/.config/rn/rn.sh"; else echo "RN shim script is missing" fi'
  grep "$init_script" ~/.bashrc &>/dev/null || \
    cat <<-EOF
		
		Your ~/.bashrc file doesn't appear to include the shim script which makes RN automatically run when you
		cd into a new directory. To add the shim, add this to your ~/.bashrc (or equivalent file):
		    $init_script
EOF

  echo
}

## Program Entry Point

function main() {
  verify_supported_os
  verify_required_tools
  download_release
  install_release
  setup_instructions
}

main
