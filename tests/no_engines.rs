#[cfg(test)]
mod no_engines {

    mod rust {
        use std::process::Command;

        #[test]
        fn code_exits_nonzero() {
            let status = Command::new("rn")
                .env("PATH", "./target/debug")
                .arg("./tests/assets/no-engines/package.json")
                .status()
                .expect("Failed to even run rn");
            assert_eq!(status.code(), Some(1i32));
        }
    }

    mod bash {
        use std::process::Command;
        use std::ffi::{OsString, OsStr};

        #[test]
        fn leaves_path_unchanged() {
            let rn_path = ::std::fs::canonicalize("./target/debug")
                .expect("Unable to canonicalize path to rn binary");
            let mut expected_path = OsString::from(rn_path);
            expected_path.push(":/bin:/usr/bin:/usr/local/bin");

            let raw_path_output = Command::new("./tests/assets/no-engines/run-rn.sh")
                .env("PATH", &expected_path)
                .output()
                .expect("Failed to even run test script");

            let stdout = String::from_utf8(raw_path_output.stdout)
                .expect("Unable to coerce bash output to string");
            let stderr = String::from_utf8(raw_path_output.stderr)
                .expect("Unable to coerce bash output to string");
            println!("stderr:\n{}\n", &stderr);
            println!("stdout:\n{}\n", &stdout);

            assert_eq!(OsStr::new(stdout.trim()), expected_path);
        }
    }

}
