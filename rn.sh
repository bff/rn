#!/bin/bash
set -o pipefail

function cd() {
  builtin cd "$@"

  local package_path
  package_path="$(pwd)/package.json"
  if [[ -f "$package_path" ]]; then
    local updated_path
    updated_path="$(rn "$package_path" | sed 's/^.*|||//')"
    if [[ $? -eq 0 ]]; then
      export PATH="$updated_path"
    fi
  fi
}
