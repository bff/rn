#!/bin/bash -eu

# Dead-code linkage needs to happen so that LLVM doesn't optimize it all away, leading
# to false high scores. THe no-run flag means we dont double-run the test.
RUSTFLAGS='-C link-dead-code' cargo test --no-run
rm -rf target/cov

# Let kcov run the test binary created above noting how much code is actually
# executed.
rm -f target/debug/*.d
kcov --exclude-pattern=/.cargo,/usr/lib --verify target/cov target/debug/rn-*

echo "Percent coverage: $(jq .percent_covered target/cov/rn-*/coverage.json)%"
